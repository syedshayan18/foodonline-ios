//
//  UIViewController+Extension.swift
//  FoodOnline
//
//  Created by mac on 11/5/18.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import JGProgressHUD
import SwiftyDrop
extension UIViewController  {
    
    func showLoader (message: String) {
        
        let hud = JGProgressHUD(style: .dark)
        
        hud.tag = 999
        hud.textLabel.text = message
        hud.show(in: self.view)
    }
    
    func dismissLoadingMessage () {
        
        if let hudView = self.view.viewWithTag(999) as? JGProgressHUD {
            
            hudView.dismiss()
        }
        
    }
    
    func shakeTextfield (textfield: UITextField, errorMessage: String) {
        
        textfield.becomeFirstResponder()
        textfield.shake()
        self.dropFailure(message: errorMessage)
        
        
    }
    
    func dropSuccess (message: String) {
        
        
                  Drop.down(message, state: CustomMessage.Success, duration: 2.0)
        //showAlert(message: message)
    }
    
    func dropFailure (message: String) {
        
        //showAlert(message: message)
         Drop.down(message, state: CustomMessage.Failure, duration: 2.0)
        
    }
    
    func showNativeAlert(message: String?, action1: (()->())? = nil,  action2: (()->())? = nil,alertStyle:UIAlertControllerStyle?) {
        
        let alert = UIAlertController(title: message, message: nil, preferredStyle: alertStyle ?? .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (alert) in
            
            if let action1 = action1 {
                
                
                action1()
            }
            if let action2 = action2 {
                
                action2()
            }
            
            
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    

    
}
extension Int
{
    func toString() -> String
    {
        
        return String(self)
    }
}
