//
//  String+Extensions.swift
//  FoodOnline
//
//  Created by mac on 11/8/18.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import Foundation
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
