//
//  Data+Extension.swift
//  FoodOnline
//
//  Created by Shayan Ali on 06/11/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//


import Foundation

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
