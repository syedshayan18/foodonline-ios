//
//  History.swift
//
//  Created by Shayan Ali on 11/11/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class History {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let orderStatus = "order_status"
    static let orderId = "order_id"
    static let restaurantName = "restaurant_name"
    static let collectionTimeFrom = "collection_time_from"
    static let restaurantId = "restaurant_id"
    static let collectionTimeTo = "collection_time_to"
    static let orderDate = "order_date"
  }

  // MARK: Properties
  public var orderStatus: String?
  public var orderId: Int?
  public var restaurantName: String?
  public var collectionTimeFrom: String?
  public var restaurantId: Int?
  public var collectionTimeTo: String?
  public var orderDate: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    orderStatus = json[SerializationKeys.orderStatus].string
    orderId = json[SerializationKeys.orderId].int
    restaurantName = json[SerializationKeys.restaurantName].string
    collectionTimeFrom = json[SerializationKeys.collectionTimeFrom].string
    restaurantId = json[SerializationKeys.restaurantId].int
    collectionTimeTo = json[SerializationKeys.collectionTimeTo].string
    orderDate = json[SerializationKeys.orderDate].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = orderStatus { dictionary[SerializationKeys.orderStatus] = value }
    if let value = orderId { dictionary[SerializationKeys.orderId] = value }
    if let value = restaurantName { dictionary[SerializationKeys.restaurantName] = value }
    if let value = collectionTimeFrom { dictionary[SerializationKeys.collectionTimeFrom] = value }
    if let value = restaurantId { dictionary[SerializationKeys.restaurantId] = value }
    if let value = collectionTimeTo { dictionary[SerializationKeys.collectionTimeTo] = value }
    if let value = orderDate { dictionary[SerializationKeys.orderDate] = value }
    return dictionary
  }

}
