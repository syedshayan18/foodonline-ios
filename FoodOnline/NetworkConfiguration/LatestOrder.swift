//
//  LatestOrder.swift
//
//  Created by mac on 11/8/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class LatestOrder {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userLong = "user_long"
    static let userAddress = "user_address"
    static let coverImage = "cover_image"
    static let userPhone = "user_phone"
    static let userLat = "user_lat"
    static let orderedPortions = "ordered_portions"
    static let userImage = "user_image"
    static let isDeal = "is_deal"
    static let dealDetails = "deal_details"
    static let collectionTimeTo = "collection_time_to"
    static let userFirstName = "user_first_name"
    static let comission = "comission"
    static let orderId = "order_id"
    static let portionCost = "portion_cost"
    static let restaurantLat = "restaurant_lat"
    static let restaurantLong = "restaurant_long"
    static let driverLat = "driver_lat"
    static let restaurantAddress = "restaurant_address"
    static let restaurantPhone = "restaurant_phone"
    static let restaurantId = "restaurant_id"
    static let userLastName = "user_last_name"
    static let dealTitle = "deal_title"
    static let driverLong = "driver_long"
    static let orderDate = "order_date"
    static let orderStatus = "order_status"
    static let centerImage = "center_image"
    static let totalCost = "total_cost"
    static let restaurantName = "restaurant_name"
    static let collectionTimeFrom = "collection_time_from"
    static let accountType = "account_type"
    static let chargeId = "charge_id"
    static let userId = "user_id"
  }

  // MARK: Properties
  public var userLong: String?
  public var userAddress: String?
  public var coverImage: String?
  public var userPhone: String?
  public var userLat: String?
  public var orderedPortions: Int?
  public var userImage: String?
  public var isDeal: Int?
  public var dealDetails: String?
  public var collectionTimeTo: String?
  public var userFirstName: String?
  public var comission: Int?
  public var orderId: Int?
  public var portionCost: Float?
  public var restaurantLat: String?
  public var restaurantLong: String?
  public var driverLat: String?
  public var restaurantAddress: String?
  public var restaurantPhone: String?
  public var restaurantId: Int?
  public var userLastName: String?
  public var dealTitle: String?
  public var driverLong: String?
  public var orderDate: String?
  public var orderStatus: String?
  public var centerImage: String?
  public var totalCost: Float?
  public var restaurantName: String?
  public var collectionTimeFrom: String?
  public var accountType: String?
  public var chargeId: String?
  public var userId: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    userLong = json[SerializationKeys.userLong].string
    userAddress = json[SerializationKeys.userAddress].string
    coverImage = json[SerializationKeys.coverImage].string
    userPhone = json[SerializationKeys.userPhone].string
    userLat = json[SerializationKeys.userLat].string
    orderedPortions = json[SerializationKeys.orderedPortions].int
    userImage = json[SerializationKeys.userImage].string
    isDeal = json[SerializationKeys.isDeal].int
    dealDetails = json[SerializationKeys.dealDetails].string
    collectionTimeTo = json[SerializationKeys.collectionTimeTo].string
    userFirstName = json[SerializationKeys.userFirstName].string
    comission = json[SerializationKeys.comission].int
    orderId = json[SerializationKeys.orderId].int
    portionCost = json[SerializationKeys.portionCost].float
    restaurantLat = json[SerializationKeys.restaurantLat].string
    restaurantLong = json[SerializationKeys.restaurantLong].string
    driverLat = json[SerializationKeys.driverLat].string
    restaurantAddress = json[SerializationKeys.restaurantAddress].string
    restaurantPhone = json[SerializationKeys.restaurantPhone].string
    restaurantId = json[SerializationKeys.restaurantId].int
    userLastName = json[SerializationKeys.userLastName].string
    dealTitle = json[SerializationKeys.dealTitle].string
    driverLong = json[SerializationKeys.driverLong].string
    orderDate = json[SerializationKeys.orderDate].string
    orderStatus = json[SerializationKeys.orderStatus].string
    centerImage = json[SerializationKeys.centerImage].string
    totalCost = json[SerializationKeys.totalCost].float
    restaurantName = json[SerializationKeys.restaurantName].string
    collectionTimeFrom = json[SerializationKeys.collectionTimeFrom].string
    accountType = json[SerializationKeys.accountType].string
    chargeId = json[SerializationKeys.chargeId].string
    userId = json[SerializationKeys.userId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userLong { dictionary[SerializationKeys.userLong] = value }
    if let value = userAddress { dictionary[SerializationKeys.userAddress] = value }
    if let value = coverImage { dictionary[SerializationKeys.coverImage] = value }
    if let value = userPhone { dictionary[SerializationKeys.userPhone] = value }
    if let value = userLat { dictionary[SerializationKeys.userLat] = value }
    if let value = orderedPortions { dictionary[SerializationKeys.orderedPortions] = value }
    if let value = userImage { dictionary[SerializationKeys.userImage] = value }
    if let value = isDeal { dictionary[SerializationKeys.isDeal] = value }
    if let value = dealDetails { dictionary[SerializationKeys.dealDetails] = value }
    if let value = collectionTimeTo { dictionary[SerializationKeys.collectionTimeTo] = value }
    if let value = userFirstName { dictionary[SerializationKeys.userFirstName] = value }
    if let value = comission { dictionary[SerializationKeys.comission] = value }
    if let value = orderId { dictionary[SerializationKeys.orderId] = value }
    if let value = portionCost { dictionary[SerializationKeys.portionCost] = value }
    if let value = restaurantLat { dictionary[SerializationKeys.restaurantLat] = value }
    if let value = restaurantLong { dictionary[SerializationKeys.restaurantLong] = value }
    if let value = driverLat { dictionary[SerializationKeys.driverLat] = value }
    if let value = restaurantAddress { dictionary[SerializationKeys.restaurantAddress] = value }
    if let value = restaurantPhone { dictionary[SerializationKeys.restaurantPhone] = value }
    if let value = restaurantId { dictionary[SerializationKeys.restaurantId] = value }
    if let value = userLastName { dictionary[SerializationKeys.userLastName] = value }
    if let value = dealTitle { dictionary[SerializationKeys.dealTitle] = value }
    if let value = driverLong { dictionary[SerializationKeys.driverLong] = value }
    if let value = orderDate { dictionary[SerializationKeys.orderDate] = value }
    if let value = orderStatus { dictionary[SerializationKeys.orderStatus] = value }
    if let value = centerImage { dictionary[SerializationKeys.centerImage] = value }
    if let value = totalCost { dictionary[SerializationKeys.totalCost] = value }
    if let value = restaurantName { dictionary[SerializationKeys.restaurantName] = value }
    if let value = collectionTimeFrom { dictionary[SerializationKeys.collectionTimeFrom] = value }
    if let value = accountType { dictionary[SerializationKeys.accountType] = value }
    if let value = chargeId { dictionary[SerializationKeys.chargeId] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    return dictionary
  }

}
