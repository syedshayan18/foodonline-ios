//
//  OrderResponse.swift
//
//  Created by Shayan Ali on 11/11/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class OrderResponse {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let sessionStatus = "session_status"
    static let history = "history"
    static let msg = "msg"
  }

  // MARK: Properties
  public var status: Bool? = false
  public var sessionStatus: Bool? = false
  public var history: [History]?
  public var msg: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    status = json[SerializationKeys.status].boolValue
    sessionStatus = json[SerializationKeys.sessionStatus].boolValue
    if let items = json[SerializationKeys.history].array { history = items.map { History(json: $0) } }
    msg = json[SerializationKeys.msg].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.status] = status
    dictionary[SerializationKeys.sessionStatus] = sessionStatus
    if let value = history { dictionary[SerializationKeys.history] = value.map { $0.dictionaryRepresentation() } }
    if let value = msg { dictionary[SerializationKeys.msg] = value }
    return dictionary
  }

}
