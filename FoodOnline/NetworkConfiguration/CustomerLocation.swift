//
//  CustomerLocation.swift
//  FoodOnline
//
//  Created by mac on 11/8/18.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import Foundation
struct CustomerLocation {
    
    let lat : Double
    let long : Double
    
}
