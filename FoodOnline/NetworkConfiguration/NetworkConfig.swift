

import AFNetworking

class NetworkConfig: NSObject {
    
    static func getRequest( route : String, baseUrl: String,param: [String:Any]?, completion:@escaping (_ data : AnyObject?, _ error : Error?) -> Void)
    {
        //network configuration
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrl))
        manager.requestSerializer.timeoutInterval = 10.0
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/json", "text/javascript","text/html", "application/x-www-form-urlencoded") as? Set<String>
        
        //get request
        manager.get(route, parameters: param, progress: nil, success: { (task, data) in
           
            //return completion with data
            completion(data as AnyObject?, nil)

        }) { (task, error) in
            //return completion with error
            completion(nil,error)
        }
        
        
    }
    
    
    static func PostRequest(token: String?, baseUrl : String, params : [String:String]?, route : String, completion:@escaping (_ data : AnyObject?,_ task: AnyObject?, _ error : Error?) -> Void)
    {
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrl))
        manager.responseSerializer.acceptableStatusCodes = nil
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.requestSerializer.setValue(token, forHTTPHeaderField: "Authorization")
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/json", "text/javascript","text/html", "application/x-www-form-urlencoded", "text/plain", "multipart/form-data" ) as? Set<String>
        
        manager.post(route, parameters: params, progress: nil, success: { (task, response) in
            
            completion(response as AnyObject?, task,nil)
        })
        { (task, error) in
            completion(nil, task,error)
            print("show error:\(error):\(task)")
        }
        
    }
    
    
 
    static func properMultipartRequestForUpload(token : String? ,baseUrl: String, fileData : [Data] , route: String, params : Any?, onSuccess:  @escaping (_ data:NSDictionary,_ task: AnyObject) -> (),  onFailure: @escaping( _ error : Error) ->Void){
        
        
       let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrl))
       
        
        manager.requestSerializer.timeoutInterval = 10
        
//        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrl))
        manager.responseSerializer.acceptableStatusCodes = nil
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/html") as? Set<String>
        

        
        manager.post(route, parameters: params, constructingBodyWith: { (formData) in
            var count = 0
            
                    for  image in fileData{
                        formData.appendPart(withFileData: image , name: "imageurl", fileName: "image", mimeType: "image/jpg")
                   
                     //   let value1 = Data(bytes: &image, count: MemoryLayout<UInt8>.size)
                        /*
                        if fileData.count == 2 {
                            
                            if count == 0 {
                                formData.appendPart(withFileData: image , name: "imageurl", fileName: "image", mimeType: "image/jpg")
                            }
                            else {
                                formData.appendPart(withFileData: image, name: "imageurltwo", fileName: "image.png", mimeType: "image/jpg")
                                
                            }
                        }
                        else if fileData.count == 1 {
                            
                            formData.appendPart(withFileData: image , name: "imageurl", fileName: "image", mimeType: "image/jpg")
                            
                        }
                        */
                       
                        
                        count+=1
                        
                        // formData.appendPart
                    }
                    formData.appendPart(withForm: "\(count)".data(using: .utf8)! , name: "count")
            
           
            
            
        }, progress: nil, success: { (task, data) in
            print("Image Upload :\(task):\(data)")
            if let object = data as? NSDictionary {
                onSuccess(object, task)
            }
        }) { (task, error) in
            print("Image Upload Error:\(task):\(error)")
            onFailure((error as NSError?)!)
        }
    }
    
    
    
    
    
    
}
