//
//  Constant.swift
//  FoodOnline
//
//  Created by Shayan Ali on 01/11/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import Foundation
import UIKit
class Constants {
     static let primaryColor = UIColor.init(red: 25/255.0, green: 168/255.0, blue: 248/255.0, alpha: 1.0)
    static let googleApiKey = "AIzaSyA8Lt2XtvjUQPpWgw6M8kuayfE3d69RomI"
    
    static let placesApiKey = "AIzaSyAJd_v6E-_fuBDfDw0kjIX9oXVGpf4NTAQ"
    static let baseUrl = "https://www.itsfoodonline.com/food_online_app/"
    //AIzaSyD9_7zCMlLckNkmoiURTSuwXCQv95ZOI_4
    static let directionApi = "AIzaSyC7lISkiaY3VnexBhXtTx4XCCAOcRc5xqI"

}
