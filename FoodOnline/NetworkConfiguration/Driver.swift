//
//  Driver.swift
//
//  Created by mac on 11/5/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
public class Driver {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let email = "email"
    static let address = "address"
    static let lat = "lat"
    static let licenseNumber = "license_number"
    static let deliveredOrders = "delivered_orders"
    static let nationalIdentification = "national_identification"
    static let lastName = "last_name"
    static let long = "long"
    static let vehicleRegistrationNumber = "vehicle_registration_number"
    static let image = "image"
    static let firstName = "first_name"
    static let phone = "phone"
    static let memberSince = "member_since"
  }

  // MARK: Properties
  public var email: String?
  public var address: String?
  public var lat: String?
  public var licenseNumber: String?
  public var deliveredOrders: Int?
  public var nationalIdentification: String?
  public var lastName: String?
  public var long: String?
  public var vehicleRegistrationNumber: String?
  public var image: String?
  public var firstName: String?
  public var phone: String?
  public var memberSince: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    email = json[SerializationKeys.email].string
    address = json[SerializationKeys.address].string
    lat = json[SerializationKeys.lat].string
    licenseNumber = json[SerializationKeys.licenseNumber].string
    deliveredOrders = json[SerializationKeys.deliveredOrders].int
    nationalIdentification = json[SerializationKeys.nationalIdentification].string
    lastName = json[SerializationKeys.lastName].string
    long = json[SerializationKeys.long].string
    vehicleRegistrationNumber = json[SerializationKeys.vehicleRegistrationNumber].string
    image = json[SerializationKeys.image].string
    firstName = json[SerializationKeys.firstName].string
    phone = json[SerializationKeys.phone].string
    memberSince = json[SerializationKeys.memberSince].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = licenseNumber { dictionary[SerializationKeys.licenseNumber] = value }
    if let value = deliveredOrders { dictionary[SerializationKeys.deliveredOrders] = value }
    if let value = nationalIdentification { dictionary[SerializationKeys.nationalIdentification] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = long { dictionary[SerializationKeys.long] = value }
    if let value = vehicleRegistrationNumber { dictionary[SerializationKeys.vehicleRegistrationNumber] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = memberSince { dictionary[SerializationKeys.memberSince] = value }
    return dictionary
  }

}
