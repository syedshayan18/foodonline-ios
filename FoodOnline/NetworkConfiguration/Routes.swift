//
//  Routes.swift
//  FoodOnline
//
//  Created by Shayan Ali on 06/11/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import Foundation

class Routes {
    static let login = "driver_signin.php"
    static let signUp = "driver_signup.php"
    static let forgetPassword = "forget_password_driver.php"
    static  let driverProfile = "driver_profile.php"
    static let newOrder = "place_order_deal_new.php"
    static let changeOrderStatus = "update_order_status.php"
    static let updateDriveLocation = "update_location_driver.php"
    static let latestOrder = "latest_order_driver.php"
    static  let orderHistory = "order_history_driver.php"
    static  let orderDetails = "order_details_restaurant.php"
    
}
