//
//  LastestOrderViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
class LatestOrderViewController: UIViewController {
    @IBOutlet weak var noLatestOrderLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var onWayButton: SpinnerButton!
    
    @IBOutlet weak var deliveredButton: SpinnerButton!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    
    @IBOutlet weak var CustomeraddressLabel: UILabel!
    
    @IBOutlet weak var customerPhoneLabel: UILabel!
    
    @IBOutlet weak var restuarantAddressLabel: UILabel!
    @IBOutlet weak var restuarantPhoneLabel: UILabel!
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var restuarantView: UIView!
    var latestOrder : LatestOrder?
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        getProfile()
        getLatestOrder()
    
        locationManager.delegate = self

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let restuarantViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(openrestuarantLocation(_:)))
        
        let customerViewtapTapGesture = UITapGestureRecognizer(target: self, action: #selector(openCustomerLocation(_:)))
        customerView.addGestureRecognizer(customerViewtapTapGesture)
        restuarantView.addGestureRecognizer(restuarantViewTapGesture)
        // Do any additional setup after loading the view.
    }
    
    private func populateLatestOrderView(latestOrder:LatestOrder) {
        
       orderIdLabel.text = latestOrder.orderId?.toString()
        dueDateLabel.text = latestOrder.orderDate
        orderStatusLabel.text = latestOrder.orderStatus
        CustomeraddressLabel.text = latestOrder.userAddress
        customerPhoneLabel.text = latestOrder.userPhone
        restuarantAddressLabel.text = latestOrder.restaurantAddress
        restuarantPhoneLabel.text = latestOrder.restaurantPhone
        

    }
    
    @IBAction func onWayButtonTapped(_ sender: SpinnerButton) {

        
        
        if let lat = CurrentDriver.shared?.lat , let long = CurrentDriver.shared?.long {
            let paramsDict = ["driver_id":CurrentDriver.userId,"lat": lat,"long":long,"session_id":CurrentDriver.token] as [String : Any]
            print(paramsDict)
            
            sender.startSpinner()
            updateRequest(paramsDict:paramsDict, route: Routes.updateDriveLocation, sender: sender)
            
            
        }
       
      
        
        
    }
    @IBAction func deliveredButtonTapped(_ sender: SpinnerButton) {
        
        let paramsDict = ["driver_id":CurrentDriver.userId,"order_id":"","order_status":1,"session_id":CurrentDriver.token] as [String : Any]
        print(paramsDict)
        sender.startSpinner()
        updateRequest(paramsDict:paramsDict, route: Routes.changeOrderStatus, sender: sender)
        
        
    }
    private func getProfile() {
        NetworkConfig.getRequest(route: Routes.driverProfile, baseUrl: Constants.baseUrl, param: ["driver_id":CurrentDriver.userId,"session_id":CurrentDriver.token]) { (response, error) in
            if error == nil {
                let json = JSON.init(response!)
                if json["status"].boolValue && json["session_status"].boolValue {
                    
                    CurrentDriver.shared = Driver(json: json["data"])
                }
                    
                else if json["session_status"].boolValue {
                     self.dropFailure(message: json["msg"].string ?? "Unable to update Profile")
                }
                else {
                    self.dropFailure(message: "Session expired please login again")
                    AppManager.shared.showLogin()
                    
                }
                
            }
            else {
               self.dropFailure(message: error?.localizedDescription ?? "Internet offline, please check your internet")
                
            }
            
        }
        
    }
   private func getLatestOrder()  {

    self.showLoader(message: "Getting Latest Order")
    NetworkConfig.getRequest(route: Routes.latestOrder, baseUrl: Constants.baseUrl, param: ["driver_id":CurrentDriver.userId,"session_id":CurrentDriver.token]) { (response, error) in
        if error == nil && response != nil {
            self.dismissLoadingMessage()

            let json = JSON.init(response!)
            
            self.noLatestOrderLabel.isHidden = false

            if json["status"].boolValue {
                
                self.noLatestOrderLabel.isHidden = true
                self.scrollView.isHidden = false
               
       self.latestOrder  = LatestOrder.init(json: json["latest_order"])
        
                 self.populateLatestOrderView(latestOrder: LatestOrder.init(json: json["latest_order"]))
                
            }
            else if json["session_status"].boolValue {
                
                 self.dropFailure(message: json["msg"].string ?? "Unable to get latest order")
               
                
            }
            else {
                AppManager.shared.showLogin()
                self.dropFailure(message: "session expired")
            }
                
            }
        
        else {
            self.dropFailure(message: error?.localizedDescription ?? "Internet offline, please check your internet")
        }
        }
      
    }

    
    
    private func updateRequest(paramsDict:[String:Any],route:String,sender:SpinnerButton) {
        
        NetworkConfig.getRequest(route: route, baseUrl: Constants.baseUrl, param: paramsDict) { (response, error) in
            sender.stopSpinner()
            if error == nil && response != nil {
                let json = JSON.init(response!)
                
                if json["status"].boolValue && json["session_status"].boolValue {
                    
                    self.dropSuccess(message: json["msg"].string ?? "status updated successfully")
                }
                else {
                    if json["session_status"].boolValue {
                         self.dropFailure(message:json["msg"].string ?? "unable to update status")
                    }
                    else {
                        AppManager.shared.showLogin()
                        self.dropFailure(message: "session expired")
                    }
                    
                    
                
                }
    
            }
            else {
                self.dropFailure(message: error?.localizedDescription ?? "Internet offline, please check your internet")
                
            }
            
        }
        
    }
    @objc func openCustomerLocation(_ sender: UITapGestureRecognizer) {
      
        if  let lat = self.latestOrder?.userLat, let long = self.latestOrder?.userLong, let _ = CurrentDriver.shared?.lat, let _ = CurrentDriver.shared?.long {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "locationVC") as! LocationViewController
            
            vc.navTitle = "Customer Location"
            vc.locationName =  "Customer"

            self.navigationController?.pushViewController(vc, animated: true)
            
            vc.retuarantLocation = RestuarantLocation.init(lat: lat.toDouble()!, long: long.toDouble()! , title: "Customer")
            
        }
        else {
self.dropFailure(message: "Unable to show location, please allow app to show your location")        }
    }
    
    @objc func openrestuarantLocation(_ sender: UITapGestureRecognizer) {
        
        
       
        
        if  let lat = self.latestOrder?.restaurantLat, let long = self.latestOrder?.restaurantLong, let _ = CurrentDriver.shared?.lat, let _ = CurrentDriver.shared?.long {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "locationVC") as! LocationViewController
            
            vc.navTitle = "Restuarant Location"
            vc.locationName =  "Restuarant"

               vc.retuarantLocation  = RestuarantLocation.init(lat: lat.toDouble()!, long: long.toDouble()! , title: "Restuarant")
            
            self.navigationController?.pushViewController(vc, animated: true)
            
         
    
        }
        else {
            self.dropFailure(message: "Unable to show location, please allow app to show your location")
        }
    }

}

extension LatestOrderViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        CurrentDriver.shared?.lat = "\(locValue.latitude)"
        CurrentDriver.shared?.long = "\(locValue.longitude)"
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        locationManager.stopUpdatingLocation()
        let alertController = UIAlertController(
            title: "Location Access Disabled",
            message: "In order to get current location status, please open this app's settings and set location access to 'Always'.",
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
        
       
    }
 
}
