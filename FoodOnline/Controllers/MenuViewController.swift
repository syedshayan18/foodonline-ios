//
//  MenuViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let MenuList = ["Home","OrderList","My Profile","About App","logout"]
    let MenuImageList = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "orderhistory"),#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "about"),#imageLiteral(resourceName: "logout")]
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        
    
        
        // Set the shadow color.
        navigationController?.navigationBar.shadowImage = Constants.primaryColor.as1ptImage()
        
        // We need to replace the navigation bar's background image as well
        // in order to make the shadowImage appear. We use the same 1px color tecnique
        

        
    }



}
extension MenuViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTableViewCell
        cell.menuLabel.text = MenuList[indexPath.row]
        cell.menuImageView.image = MenuImageList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            self.performSegue(withIdentifier: "gotoLatestOrder", sender: nil)
        }
        else if indexPath.row == 1 {
            self.performSegue(withIdentifier: "gotoOrderHistory", sender: nil)

        }
        else if indexPath.row == 2  {
            if let _ = CurrentDriver.shared {

                self.performSegue(withIdentifier: "gotoMyProfile", sender: nil)
            }
            else {
                self.dropFailure(message: "unable to show profile right now.")
            }

        }
        
        else if indexPath.row == 3 {
            self.performSegue(withIdentifier: "gotoAbout", sender: nil)

        }
        else if indexPath.row == 4 {
            AppManager.shared.showLogin()

        }
        
    }
}
