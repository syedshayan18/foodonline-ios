//
//  MyProfileViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import SDWebImage
class MyProfileViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var memberSinceLabel: UILabel!
    @IBOutlet weak var deliveredCountLabel: UILabel!
    @IBOutlet weak var registrationNumLabel: UILabel!
    @IBOutlet weak var vehicleLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        driverNameLabel.text = "\(CurrentDriver.shared!.firstName!) \(CurrentDriver.shared!.lastName!)"
        memberSinceLabel.text = CurrentDriver.shared?.memberSince!
        registrationNumLabel.text = CurrentDriver.shared?.vehicleRegistrationNumber!
        deliveredCountLabel.text =  CurrentDriver.shared?.deliveredOrders?.toString()
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
