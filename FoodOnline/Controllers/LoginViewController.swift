
//
//  ViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 30/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var loginButton: SpinnerButton!
    override func viewDidLoad() {
        super.viewDidLoad()
//        emailTextField.text = "s@s.com"
//        passwordTextField.text = "123456"
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func loginButtonTapped(_ sender: SpinnerButton) {
        
        if let error = Validator.validateEmptyTextFields(textFields: [emailTextField,passwordTextField]) {
            
            self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
        }
        else if let error = Validator.validateEmail(textField: emailTextField){
            
            self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
        }
        else {
            let params = ["device_type":"ios","device_token":CurrentDriver.deviceToken,"email":emailTextField.text!,"password":passwordTextField.text!]
            sender.startSpinner()
      
            NetworkConfig.properMultipartRequestForUpload(token: nil, baseUrl: Constants.baseUrl, fileData: [], route: Routes.login, params: params, onSuccess: { (response, task) in
                sender.stopSpinner()
                
                if let success = response["status"] as? Bool, success, let sessionStatus = response["session_status"] as? Bool, sessionStatus {
                    
                    CurrentDriver.token =  response["session_id"] as! String
                    CurrentDriver.userId = response["id"] as! Int
                    UserDefaults.standard.set(CurrentDriver.token, forKey: "token")
                    UserDefaults.standard.set(CurrentDriver.userId, forKey: "userId")
                        self.performSegue(withIdentifier: "homeVC", sender: nil)
                }
                else {
                    self.dropFailure(message: response["msg"] as? String ?? "Error in login. Please try again")
                    
                }
                
                
            }) { (error) in
                
                self.dropFailure(message: error.localizedDescription)
            }
           
            
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true

    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

