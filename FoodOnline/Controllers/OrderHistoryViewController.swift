//
//  OrderHistoryViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import SwiftyJSON
class OrderHistoryViewController: UIViewController {
    @IBOutlet weak var noOrderHistoryLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var orderhistoryList : [History]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        getOrderHistory()
    }
    
    private func getOrderHistory() {
        
        self.showLoader(message: "Getting History")
       // ["driver_id":CurrentDriver.userId,"session_id":CurrentDriver.token]
        NetworkConfig.getRequest(route: Routes.orderHistory, baseUrl: Constants.baseUrl, param:["driver_id":CurrentDriver.userId,"session_id":CurrentDriver.token]) { (response, error) in
            if error == nil && response != nil {
                self.dismissLoadingMessage()
                self.noOrderHistoryLabel.isHidden = false

                let json = JSON.init(response!)
                if json["status"].boolValue && json["session_status"].boolValue {
                    
                   let orderResponse = OrderResponse.init(json: json)
                    
                    self.orderhistoryList = orderResponse.history
                    self.tableView.reloadData()
                    self.tableView.isHidden = false
                    self.noOrderHistoryLabel.isHidden = true
                }
                else {
                    if json["session_status"].boolValue {
                        
                        
                        self.dropFailure(message:json["msg"].string ?? "unable to get History")
                    }
                        
                    else {
                        AppManager.shared.showLogin()
                        self.dropFailure(message: "session expired")
                    }
                }
        
                
            }
            else {
                self.dropFailure(message: error?.localizedDescription ?? "Internet offline, please check your internet")
                
            }
            
        }
    }
    
    
}
extension OrderHistoryViewController: UITableViewDelegate,UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let orderHistory = orderhistoryList else{
             return 0
        }
        
        return orderHistory.count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderHistoryTableViewCell
        
        cell.orderIdLabel.text = "\(orderhistoryList![indexPath.row].orderId ?? 0)"
        cell.statusLabel.text = orderhistoryList![indexPath.row].orderStatus ?? "NA"
        cell.dueDateLabel.text = orderhistoryList![indexPath.row].orderDate  ?? "NA"
        
        
        
        return cell
    }
    
}
