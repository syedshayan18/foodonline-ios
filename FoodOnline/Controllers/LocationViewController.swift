//
//  LocationViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
class LocationViewController: UIViewController {
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var infoView: UIView!
    var navTitle : String?
    var zoom: Float = 12
    var retuarantLocation : RestuarantLocation!
    var locationName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.text = "Below map shows the route from your current location to the \(locationName)"
        let camera = GMSCameraPosition.camera(withLatitude: Double(CurrentDriver.shared!.lat!)!,
                                              longitude: Double(CurrentDriver.shared!.long!)! ,
                                              zoom: 12.0,
                                              bearing: 30,
                                              viewingAngle: 40)
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
 
        self.navigationItem.title = navTitle
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: self.view.frame.size.height - 150, right: 10)
        // Do any additional setup after loading the view.
        getDirectionandDrawPath()
    }
    
    private func getDirectionandDrawPath() {
        
        let origin = "\(CurrentDriver.shared!.lat!),\(CurrentDriver.shared!.long!)"
        let destination = "\(retuarantLocation.lat),\(retuarantLocation.long)"
        
        let baseUrl = "https://maps.googleapis.com/maps/api/"
        let paramsDict = ["origin":origin,"destination":destination,"mode":"driving","key":Constants.directionApi]
        NetworkConfig.getRequest(route: "directions/json", baseUrl: baseUrl, param: paramsDict) { (response, error) in
            
            let json = JSON.init(response!)
            let routes = json["routes"].arrayValue
            
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyline = GMSPolyline.init(path: path)
                polyline.strokeColor = Constants.primaryColor
                polyline.strokeWidth = 2
                polyline.map = self.mapView
            }
        }
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(CurrentDriver.shared!.lat!)!, longitude: Double(CurrentDriver.shared!.long!)!)
        marker.title = "Me"
        marker.icon = UIImage.init(named: "location_car_icon")
        marker.map = mapView
        
        //28.643091, 77.218280
        let marker1 = GMSMarker()
//        marker1.position = CLLocationCoordinate2D(latitude: retuarantLocation.lat, longitude: retuarantLocation.long)
         marker1.position = CLLocationCoordinate2D(latitude: retuarantLocation.lat, longitude: retuarantLocation.long)
        marker1.title = retuarantLocation.title
        marker1.icon = UIImage.init(named: retuarantLocation.title)
        
        marker1.map = mapView
    }

    @IBAction func closeButtonTapped(_ sender: Any) {
        infoView.isHidden = true;
        
    }
    
  
    @IBAction func plusZoomButtonTapped(_ sender: UIButton) {
        zoom  += 1
        self.mapView.animate(toZoom: zoom)
    }
    
    
    @IBAction func minusZoomButtonTapped(_ sender: UIButton) {
        zoom -= 1
        self.mapView.animate(toZoom: zoom)
    }
    

}

