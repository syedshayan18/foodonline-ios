//
//  SignUpViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
class SignUpViewController: UIViewController {
 
  
    @IBOutlet weak var signUpButton: SpinnerButton!
    
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var drivingLisenceTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var NICTextField: SkyFloatingLabelTextField!

    @IBOutlet weak var addressTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var vehicleRegistrationTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
      var imagePicker = UIImagePickerController()
    var imageData = Data()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self


    }
    @IBAction func deliveredButtonTapped(_ sender: Any) {
    }
    
    @IBAction func addImageButtonTapped(_ sender: UIButton) {
        
       
        let mediaAlert = UIAlertController(title: "Select Picture", message: "Media Options", preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: "Library", style: .default) { (action:UIAlertAction) in
           
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction) in
            
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let removePicture = UIAlertAction(title: "Remove Picture", style: .destructive) { (action:UIAlertAction) in
            self.profileImageView.image = nil
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
           
            
        }
        
        mediaAlert.addAction(library)
        mediaAlert.addAction(camera)
        mediaAlert.addAction(removePicture)
        mediaAlert.addAction(cancel)
        
        self.present(mediaAlert, animated: true, completion: nil)
    }
    
    @IBAction func SignUpButtonTapped(_ sender: SpinnerButton) {
        
        
        if let error =  Validator.validateEmptyTextFields(textFields: [firstNameTextField,lastNameTextField,phoneNumberTextField,drivingLisenceTextField,NICTextField,addressTextField,vehicleRegistrationTextField,emailTextField,passwordTextField]) {
            
            self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
            
        }
        else if let error = Validator.validateEmail(textField: emailTextField) {
            
            self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
        }
        else  if let error = Validator.validatePasswordTextField(textField: passwordTextField) {
            
            self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
        }
        else {
            sender.startSpinner()
           let params = ["first_name":firstNameTextField.text!,"last_name":lastNameTextField.text!,"license_number":drivingLisenceTextField.text!,"phone":phoneNumberTextField.text!,"email":emailTextField.text!,"password":passwordTextField.text!,"identification_number":NICTextField.text!,"address":addressTextField.text!,"registration_number":vehicleRegistrationTextField.text!]
            NetworkConfig.properMultipartRequestForUpload(token: nil, baseUrl: Constants.baseUrl, fileData: [imageData], route: Routes.signUp, params: params, onSuccess: { (response, task) in
                sender.stopSpinner()
                
                if let success = response["status"] as? Bool, success {
                    
                    self.navigationController?.popViewController(animated: true)
                     self.dropSuccess(message: response["msg"]  as? String ?? "Registered Successfully!")
                }
                else {
                    
                    self.dropFailure(message: response["msg"] as? String ?? "Error in Signup. Please try again")
                    
                }
                
                
            }) { (error) in
                
                self.dropFailure(message: error.localizedDescription)
            }
        }
        
      
    }
    
 
    @IBAction func backToLoginTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: Pressed Functions
extension SignUpViewController  {
    
    @IBAction func openCameraTapped(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
}

// MARK: Pressed ImagePickerDelegate
extension SignUpViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImageView.image = image
            guard let imageData = UIImageJPEGRepresentation(image, 0.1) else {return}
            self.imageData = imageData
            
        }
        
        picker.dismiss(animated: true, completion: nil);
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
}
