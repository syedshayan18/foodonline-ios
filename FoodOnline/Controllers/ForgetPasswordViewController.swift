//
//  ForgetPasswordViewController.swift
//  FoodOnline
//
//  Created by Shayan Ali on 31/10/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
class ForgetPasswordViewController: UIViewController {
    @IBOutlet weak var resetPasswordButton: SpinnerButton!
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func resetPasswordTapped(_ sender: SpinnerButton) {
        
        if let error = Validator.validateEmptyTextFields(textFields: [emailTextField]){
            self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
        }
        else if let error = Validator.validateEmail(textField: emailTextField) {
             self.shakeTextfield(textfield: error.textField, errorMessage: error.localizedDescription)
        }
        else {
            
            sender.startSpinner()
//             let email = emailTextField.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//            print(email)
            let params = ["email":emailTextField.text!]
          //  let route = "\(Routes.forgetPassword)?email=\(emailTextField.text!)"
            NetworkConfig.getRequest(route: Routes.forgetPassword, baseUrl: Constants.baseUrl, param: params) { (response, error) in
                sender.stopSpinner()
                print(response)
                if error == nil {
                    let json = JSON.init(response!)
                    print(json)
                    if json["status"].boolValue {
                        
                       self.dropSuccess(message: json["msg"].string ?? "Password reset, Please check your mail")
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                      self.dropFailure(message: json["msg"].string ?? "Error in resetting password, Please try again")
                    }
                    
                }
                else {
                    self.dropFailure(message: error?.localizedDescription ?? "Error in resetting password, Please try again" )
                }
            }
        }
    }
  


}
