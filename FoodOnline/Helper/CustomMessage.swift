//
//  CustomMessage.swift
//  FoodPic
//
//  Created by mac on 7/31/18.
//  Copyright © 2018 Invision. All rights reserved.
//

import Foundation
import SwiftyDrop

enum CustomMessage: DropStatable {
    
    
   
    
     case Success
     case Failure
    

    
    var backgroundColor: UIColor? {
        switch self {
        case .Success: return UIColor.green
            case .Failure: return UIColor.red
        }
    }
    var font: UIFont? {
        switch self {
        case .Success,.Failure: return UIFont(name: "Poppins-Regular", size: 16.0)
        }
    }
    var textColor: UIColor? {
        switch self {
        case .Success,.Failure: return UIColor.white
        }
    }
    var blurEffect: UIBlurEffect? {
        switch self {
        case .Success,.Failure: return nil
        }
    }
    var textAlignment: NSTextAlignment? {
        
        
        switch self {
        case .Success,.Failure:
            return NSTextAlignment.center
            
        }
        
    }
}
