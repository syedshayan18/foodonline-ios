//
//  AppManager.swift
//  FoodOnline
//
//  Created by Shayan Ali on 01/11/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

//
//  AppManager.swift
//  RAD
//
//  Created by Invision on 08/05/2018.
//  Copyright © 2018 Invision. All rights reserved.
//

import UIKit

enum StatusError: Int{
    case notAuthorized = 400
    
    func desc()->String{
        switch self{
        case .notAuthorized:
            return "You are not authorized to login at this time."
        }
    }
}

class AppManager: NSObject {
    
    static var shared = AppManager()
    private override init() {}
    
    
    var window:UIWindow?? {
        return UIApplication.shared.delegate?.window
    }
    
    func handleTokenExpiry(){
        let vc = UIApplication.topViewController()
        /*
        vc?.showAlert(title: "Session expired", message: "Please login again to continue", completion:{
            
            
            self.showLogin()
        } )
 */
        
    }
    
    func showMain(){
        let mainVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "homeVC")
        window??.rootViewController = mainVC
    }
    
    func showLogin(){
        clear(window: window)
        let signinVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "LoginNavigationViewController")
        window??.rootViewController = signinVC
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "userId")
        CurrentDriver.shared = nil
        
    }
    /*
    func resumeSetup(vcIdentifier: String) {
        
        let vc = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier:"")
        window??.rootViewController = vc
        
    }
 */
    
    func clear(window: UIWindow??) {
        window??.subviews.forEach { $0.removeFromSuperview() }
    }
    
    
    
}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


