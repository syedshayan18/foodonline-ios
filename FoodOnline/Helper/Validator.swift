//
//  Validator.swift
//
//  Created by Invision on 09/08/2017.
//  Copyright © 2017 Invision. All rights reserved.
//

import UIKit

protocol ErrorProtocol: Error {
    var localizedTitle: String { get }
    var localizedDescription: String { get }
}

protocol FieldErrorProtocol: ErrorProtocol{
    var textField:UITextField { get }
}


struct CustomError: ErrorProtocol{
    var localizedTitle: String
    var localizedDescription: String
    
    init(localizedTitle: String? = "Error", localizedDescription: String) {
        self.localizedTitle = localizedTitle!
        self.localizedDescription = localizedDescription
    }
}

/**
 Field Error is used in TextField validations. The error will contain the textfield which caused the error.
 */
struct FieldError: FieldErrorProtocol {
    
    var localizedTitle: String
    var localizedDescription: String
    var textField: UITextField
    
    init(localizedTitle: String? = "Error", localizedDescription: String, textField:UITextField) {
        self.localizedTitle = localizedTitle!
        self.localizedDescription = localizedDescription
        self.textField=textField
    }
}

class Validator: NSObject {
    
    
    /**
     Validates Empty Text fields
     
     -parameter textFields: Array of textfields to validate
     -returns: A field error if any text field is empty.
    */
    class func validateEmptyTextFields(textFields:[UITextField])->FieldError?{
        
        for textField in textFields{
            if(textField.text!.isEmpty){
                return FieldError(localizedDescription: "\(textField.placeholder!) cannot be empty", textField: textField)
            }
        }
        
        return nil
    }
    
    /**
     Validates Password text field, checks if character count is greater than 6
     
     -parameter textField: Textfield to validate
     -returns: A field error if any case is not satisfied
     */
    class func validatePasswordTextField(textField:UITextField)->FieldError?{
        
        if(textField.text!.characters.count < 6){
            return FieldError(localizedDescription: "Password cannot be less then 6 characters", textField: textField)
        }
        
        return nil
        
    }
    
    /**
     Validates Email text field, checks if email is in the right format
     
     -parameter textFields: Textfield to validate
     -returns: A field error if email format is wrong
     */
    class func validateEmail(textField:UITextField)->FieldError?{
        let emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        if (textField.text?.range(of: emailRegex, options: .regularExpression) == nil){
            return FieldError(localizedDescription: "Invalid Email", textField: textField)
        }
        return nil
    }
    
    /**
     Validates Password and Confirm password Text fields
     
     -parameter textFields: Array of textfields to compare
     -returns: A field error if text does not match.
     */
    class func comparePasswordTextFields(textFields:[UITextField])->FieldError?{
        
        if(textFields.first?.text != textFields.last?.text){
            return FieldError(localizedDescription: "Passwords do not match", textField: textFields.last!)
        }
        
        return nil
    }
    

}
